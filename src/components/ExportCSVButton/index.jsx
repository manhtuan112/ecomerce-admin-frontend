import React from 'react'
import { CSVLink } from 'react-csv'
import { CiExport } from 'react-icons/ci'
export const ExportCSVButton = ({ csvData, fileName }) => {
  const data = csvData.map((item) => {
    const { action, ...rest } = item
    if (rest.thumbnail) {
      rest.thumbnail = rest.thumbnail.props.src
    }
    if (rest.image){
      rest.image = rest.image.props.src
    }
    return rest
  })
  return (
    <button className='btn btn-success ms-3'>
      <CSVLink data={data} filename={fileName} className='text-white fw-bold text-decoration-none'>
        <CiExport className='fs-4' />
        Export File
      </CSVLink>
    </button>
  )
}
