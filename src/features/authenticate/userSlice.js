import { createSlice } from '@reduxjs/toolkit'
import { loginMethod } from "./middlewate";
import { setLocalStorage } from '../../utils/localStorage';
const userStateType = {
  username: null,
  avatar: null
}

const initialState = {
  user: userStateType,
  isError: false,
  isLoading: false,
  isSuccess: false,
  message: "",
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {

  },
  extraReducers: (builder) => {
    builder
      // Login
      .addCase(loginMethod.pending, (state) => {
        state.isLoading = true
      })
      .addCase(loginMethod.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.user = action.payload.data
        setLocalStorage('adminAccessToken', action.payload.tokens.access);
      })
      .addCase(loginMethod.rejected, (state) => {
        state.isLoading = false
        state.isSuccess = false
        state.isError = true
        state.user = null
        state.message = 'Rejected'
      })


  }
})

// export const { setLogin, setSignup, resetState } = userSlice.actions
export default userSlice.reducer