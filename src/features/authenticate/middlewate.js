import { createAsyncThunk } from '@reduxjs/toolkit';
import { AuthApiMethod } from './api';

export const loginMethod = createAsyncThunk(
  'auth/loginMethod',
  async (data, thunkAPI) => {
    try {
      const response = await AuthApiMethod.login(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);