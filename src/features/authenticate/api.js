import {createAxiosInstance} from '../../utils/axiosInstance'

export const AuthApiMethod = {
  baseUrl: import.meta.env.VITE_API_URL || process.env.REACT_APP_API_URL,
  login: (data) => { 
    const axiosInstance = createAxiosInstance(AuthApiMethod.baseUrl)
    return axiosInstance.post('/user/auth/admin/login/', data)
  },

}