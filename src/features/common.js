export const getCurrentDateTimeFormatted = () => {
  const now = new Date();

  // Get day, month, and year components
  const day = String(now.getDate()).padStart(2, '0');
  const month = String(now.getMonth() + 1).padStart(2, '0');
  const year = String(now.getFullYear());

  // Get seconds, minutes, and hours components
  const seconds = String(now.getSeconds()).padStart(2, '0');
  const minutes = String(now.getMinutes()).padStart(2, '0');
  const hours = String(now.getHours()).padStart(2, '0');

  // Concatenate the components in the desired format
  const formattedDateTime = day + month + year + seconds + minutes + hours;

  return formattedDateTime;
}