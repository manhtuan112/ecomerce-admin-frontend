import {createAxiosInstance} from '../../utils/axiosInstance'

export const ProductApiMethod = {
  baseUrl: import.meta.env.VITE_API_URL_PRODUCT || process.env.REACT_APP_API_URL,
  getListProduct: () => {
    const axiosInstance = createAxiosInstance(ProductApiMethod.baseUrl)
    return axiosInstance.get('/product/get_all_admin/')
  },
  deleteProduct: (data) => {
    const axiosInstance = createAxiosInstance(ProductApiMethod.baseUrl)
    return axiosInstance.delete(`/product/delete/${data}/`)
  },



  // common api aplly for brand, category, collection
  createItems: (data, type) => {
    const axiosInstance = createAxiosInstance(ProductApiMethod.baseUrl)
    return axiosInstance.post(`/items/create/${type}/`, data)
  },

  deleteItems: (data) => {
    const axiosInstance = createAxiosInstance(ProductApiMethod.baseUrl)
    return axiosInstance.delete(`/items/delete/${data.type}/${data.id}/`)
  },

  getListItems: (data) => {
    const axiosInstance = createAxiosInstance(ProductApiMethod.baseUrl)
    return axiosInstance.get(`/items/get_all/${data.type}/`)
  },

  getItems: (data) => {
    const axiosInstance = createAxiosInstance(ProductApiMethod.baseUrl)
    return axiosInstance.get(`/items/get/${data.type}/${data.id}/`)
  },

  updateItems: (data, params) => {
    const axiosInstance = createAxiosInstance(ProductApiMethod.baseUrl)
    return axiosInstance.put(`/items/update/${params.type}/${params.id}/`, data)
  }


}