import { createSlice } from '@reduxjs/toolkit'
import { createItemsMethod, deleteItemsMethod, deleteProductMethod, getItemsMethod, getListItemsMethod, getListProductMethod, updateItemsMethod } from "./middlewate";

const initialState = {
  isError: false,
  isLoading: false,
  isSuccess: false,
  // aplly for brand, category, collection
  listItems: [],
  ErrorMessage: '',
  typeOfItems: ''
};

export const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    setTypeofItems: (state, action) => {
      state.typeOfItems = action.payload
    }
  },
  extraReducers: (builder) => {
    builder

      // getListItems
      .addCase(getListProductMethod.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getListProductMethod.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.listItems = action.payload.data

      })
      .addCase(getListProductMethod.rejected, (state) => {
        state.isLoading = false
        state.isSuccess = false
        state.isError = true
      })

      // deleteProduct
      .addCase(deleteProductMethod.pending, (state) => {
        state.isLoading = true
      })
      .addCase(deleteProductMethod.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true

      })
      .addCase(deleteProductMethod.rejected, (state) => {
        state.isLoading = false
        state.isSuccess = false
        state.isError = true
      })


      // createItems
      .addCase(createItemsMethod.pending, (state) => {
        state.isLoading = true
      })
      .addCase(createItemsMethod.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true

      })
      .addCase(createItemsMethod.rejected, (state) => {
        state.isLoading = false
        state.isSuccess = false
        state.isError = true
      })

      // deleteItems
      .addCase(deleteItemsMethod.pending, (state) => {
        state.isLoading = true
      })
      .addCase(deleteItemsMethod.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true

      })
      .addCase(deleteItemsMethod.rejected, (state) => {
        state.isLoading = false
        state.isSuccess = false
        state.isError = true
      })

      // getItems
      .addCase(getItemsMethod.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getItemsMethod.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true

      })
      .addCase(getItemsMethod.rejected, (state) => {
        state.isLoading = false
        state.isSuccess = false
        state.isError = true
      })

      // getListItems
      .addCase(getListItemsMethod.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getListItemsMethod.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.listItems = action.payload.data

      })
      .addCase(getListItemsMethod.rejected, (state) => {
        state.isLoading = false
        state.isSuccess = false
        state.isError = true
      })

      // updateItems
      .addCase(updateItemsMethod.pending, (state) => {
        state.isLoading = true
      })
      .addCase(updateItemsMethod.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true

      })
      .addCase(updateItemsMethod.rejected, (state) => {
        state.isLoading = false
        state.isSuccess = false
        state.isError = true
      })

  }
})

export const { setTypeofItems } = productSlice.actions
export default productSlice.reducer