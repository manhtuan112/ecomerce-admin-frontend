import { createAsyncThunk } from '@reduxjs/toolkit';
import { ProductApiMethod } from './api';

export const getListProductMethod = createAsyncThunk(
  'product/getListProductMethod',
  async (data, thunkAPI) => {
    try {
      const response = await ProductApiMethod.getListProduct();
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const deleteProductMethod = createAsyncThunk(
  'product/deleteProductMethod',
  async (data, thunkAPI) => {
    try {
      const response = await ProductApiMethod.deleteProduct(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);




export const getDetailProduct = createAsyncThunk(
  'product/getDetailProduct',
  async (data, thunkAPI) => {
    try {
      const response = await ProductApiMethod.getDetailProduct(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);


// common api aplly for brand, category, collection
export const createItemsMethod = createAsyncThunk(
  'items/createItemsMethod',
  async ({data, type}, thunkAPI) => {
    try {
      const response = await ProductApiMethod.createItems(data, type);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message[0])
    }

  }
);


export const deleteItemsMethod = createAsyncThunk(
  'items/deleteItemsMethod',
  async (data, thunkAPI) => {
    try {
      const response = await ProductApiMethod.deleteItems(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const getItemsMethod = createAsyncThunk(
  'items/getItemsMethod',
  async (data, thunkAPI) => {
    try {
      const response = await ProductApiMethod.getItems(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const getListItemsMethod = createAsyncThunk(
  'items/getListItemsMethod',
  async (data, thunkAPI) => {
    try {
      const response = await ProductApiMethod.getListItems(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const updateItemsMethod = createAsyncThunk(
  'items/updateItemsMethod',
  async ({data, params},  thunkAPI) => {
    try {
      const response = await ProductApiMethod.updateItems(data, params);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }
  }
);