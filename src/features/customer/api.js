import {createAxiosInstance} from '../../utils/axiosInstance'

export const CustomerApiMethod = {
  baseUrl: import.meta.env.VITE_API_URL || process.env.REACT_APP_API_URL,
  getListCustomer: () => { 
    const axiosInstance = createAxiosInstance(CustomerApiMethod.baseUrl)
    return axiosInstance.get('/user/get_all_user/')
  },

  deleteCustomer: (data) => { 
    const axiosInstance = createAxiosInstance(CustomerApiMethod.baseUrl)
    return axiosInstance.delete(`/user/delete/${data}/`)
  },

  addCustomer: (data) => { 
    const axiosInstance = createAxiosInstance(CustomerApiMethod.baseUrl)
    return axiosInstance.post(`/user/create/`, data)
  },

  getCustomerDetail: (data) => { 
    const axiosInstance = createAxiosInstance(CustomerApiMethod.baseUrl)
    return axiosInstance.get(`user/admin_profile/${data}/`)
  },

  updateCustomerDetail: (data, usename) => { 
    console.log(data, usename);
    const axiosInstance = createAxiosInstance(CustomerApiMethod.baseUrl)
    return axiosInstance.put(`user/admin_update_profile/${usename}/`, data)
  },

}