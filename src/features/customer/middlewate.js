import { createAsyncThunk } from '@reduxjs/toolkit';
import { CustomerApiMethod } from './api';

export const getAllCustomerMethod = createAsyncThunk(
  'customer/getAllCustomerMethod',
  async (data, thunkAPI) => {
    try {
      const response = await CustomerApiMethod.getListCustomer();
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const deleteCustomerMethod = createAsyncThunk(
  'customer/deleteCustomerMethod',
  async (data, thunkAPI) => {
    try {
      const response = await CustomerApiMethod.deleteCustomer(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const addCustomerMethod = createAsyncThunk(
  'customer/addCustomerMethod',
  async (data, thunkAPI) => {
    try {
      const response = await CustomerApiMethod.addCustomer(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const getCustomerDetailMethod = createAsyncThunk(
  'customer/getCustomerDetailMethod',
  async (data, thunkAPI) => {
    try {
      const response = await CustomerApiMethod.getCustomerDetail(data);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);

export const updateCustomerDetailMethod = createAsyncThunk(
  'customer/updateCustomerDetailMethod',
  async ({data, username}, thunkAPI) => {
    try {
      const response = await CustomerApiMethod.updateCustomerDetail(data, username);
      return response.data;
    } catch (error) {
      console.log(error);
      return thunkAPI.rejectWithValue(error.response.data.message)
    }

  }
);