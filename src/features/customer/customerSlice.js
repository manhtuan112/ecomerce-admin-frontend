import { createSlice } from '@reduxjs/toolkit'
import { addCustomerMethod, deleteCustomerMethod, getAllCustomerMethod, getCustomerDetailMethod, updateCustomerDetailMethod } from "./middlewate";

const initialState = {
  isError: false,
  isLoading: false,
  isSuccess: false,
  listCustomer: [],
  ErrorMessage: '',
  selectedKey: ''
};

export const customerSlice = createSlice({
  name: 'customer',
  initialState,
  reducers: {
    setSelectedKey: (state, action) => {
      state.selectedKey = action.payload
    },
  },
  extraReducers: (builder) => {
    builder
      // GetListCustomer
      .addCase(getAllCustomerMethod.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getAllCustomerMethod.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.listCustomer = action.payload.data
      })
      .addCase(getAllCustomerMethod.rejected, (state) => {
        state.isLoading = false
        state.isSuccess = false
        state.isError = true
      })

      // deleteCustomer
      .addCase(deleteCustomerMethod.pending, (state) => {
        state.isLoading = true
      })
      .addCase(deleteCustomerMethod.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
      })
      .addCase(deleteCustomerMethod.rejected, (state) => {
        state.isLoading = false
        state.isSuccess = false
        state.isError = true
      })

      // addCustomer
      .addCase(addCustomerMethod.pending, (state) => {
        state.isLoading = true
      })
      .addCase(addCustomerMethod.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
      })
      .addCase(addCustomerMethod.rejected, (state) => {
        state.isLoading = false
        state.isSuccess = false
        state.isError = true
      })

      // getCustomerDetail
      .addCase(getCustomerDetailMethod.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getCustomerDetailMethod.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
      })
      .addCase(getCustomerDetailMethod.rejected, (state) => {
        state.isLoading = false
        state.isSuccess = false
        state.isError = true
      })

      // updateCustomerDetail
      .addCase(updateCustomerDetailMethod.pending, (state) => {
        state.isLoading = true
        state.isSuccess = false
        state.isError = false
        state.ErrorMessage = ''
      })
      .addCase(updateCustomerDetailMethod.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.ErrorMessage = ''
      })
      .addCase(updateCustomerDetailMethod.rejected, (state, action) => {
        state.isLoading = false
        state.isSuccess = false
        state.isError = true
        state.ErrorMessage = action.payload
      })


  }
})

export const { setSelectedKey } = customerSlice.actions
export default customerSlice.reducer