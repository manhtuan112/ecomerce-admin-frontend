import { React, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import 'react-quill/dist/quill.snow.css'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import * as yup from 'yup'
import { useFormik } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import './style.scss'
import CustomInput from '../../components/CustomInput'
import { storage } from '../../../firebaseConfig'
import { getDownloadURL, ref, uploadBytes } from 'firebase/storage'
import { v4 } from 'uuid'
import { getCurrentDateTimeFormatted } from '../../features/common'
import { addCustomerMethod } from '../../features/customer/middlewate'
import Loading from '../loading'
let schema = yup.object().shape({
  username: yup.string().required('Title is Required'),
  email: yup.string().required('Email is Required'),
  last_name: yup.string().required('LastName is Required'),
  first_name: yup.string().required('FisrtName is Required'),
  date_of_birth: yup.string().required('Date of Birth is Required'),
  password: yup.string().required('Password is Required'),
  telephoneNumber: yup.string().required('Telephone is Required'),
  role: yup.string().required('Role is Required'),
  street: yup.string().required('Street is Required'),
  city: yup.string().required('City is Required'),
  state: yup.string().required('State is Required'),
  home_number: yup.string().required('Home Number is Required')
})

const AddCustomer = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const [selectedImage, setSelectedImage] = useState('')
  const [isLoading, setIsLoading] = useState(false)

  const uploadImage = () => {
    return new Promise((resolve, reject) => {
      if (selectedImage === null) return reject('No image selected')
      const imageRef = ref(storage, `avatar/${v4() + '_' + selectedImage.name + '_' + getCurrentDateTimeFormatted()}`)

      uploadBytes(imageRef, selectedImage)
        .then((res) => {
          getDownloadURL(res.ref)
            .then((url) => {
              resolve(url)
            })
            .catch((e) => {
              reject(e)
            })
        })
        .catch((e) => {
          reject(e)
        })
    })
  }

  const formik = useFormik({
    initialValues: {
      username: '',
      email: '',
      last_name: '',
      first_name: '',
      password: '',
      date_of_birth: '',
      telephoneNumber: '',
      role: 'NORMAL_USER',
      street: '',
      city: '',
      state: '',
      home_number: ''
    },
    validationSchema: schema,
    onSubmit: (values) => {
      handleAddCustomer()
    }
  })

  const handleAddCustomer = async () => {
    setIsLoading(true)
    try {
      const url = await uploadImage()
      const data = {
        username: formik.values.username,
        email: formik.values.email,
        last_name: formik.values.last_name,
        first_name: formik.values.first_name,
        password: formik.values.password,
        date_of_birth: formik.values.date_of_birth,
        telephoneNumber: formik.values.telephoneNumber,
        role: formik.values.role,
        Address: {
          street: formik.values.street,
          city: formik.values.city,
          state: formik.values.state,
          home_number: formik.values.home_number
        },
        avatar: url
      }
      dispatch(addCustomerMethod(data))
        .then(() => {
          toast.success('Add customer successful', {
            position: 'top-right',
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: true,
            theme: 'light'
          })
          setIsLoading(false)
          setTimeout(() => {
            navigate('/admin/customers')
          }, 1000)
        })
        .catch((err) => {
          toast.error(`Delete customer faled: ${err}`, {
            position: 'top-right',
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: true,
            theme: 'light'
          })
        })
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div>
      <ToastContainer />
      {isLoading && <Loading />}
      <h3 className='mb-4 username'>Add Customer</h3>
      <div>
        <img
          className='img-account-profile rounded-circle mb-2 img-fluid'
          src={
            selectedImage
              ? URL.createObjectURL(selectedImage)
              : 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png'
          }
          alt='image'
        />
        <button className='btn btn-primary' type='button'>
          <label htmlFor='upload image'>Upload new image</label>
        </button>
        <input
          type='file'
          name='myImage'
          id='upload image'
          className='d-none'
          onChange={(event) => {
            setSelectedImage(event.target.files[0])
          }}
        />
        <form onSubmit={formik.handleSubmit} className='d-flex gap-3 flex-column'>
          <div className='d-flex gap-5'>
            <div className='w-50'>
              <CustomInput
                type='text'
                label='Enter First Name'
                name='first_name'
                onChng={formik.handleChange('first_name')}
                onBlr={formik.handleBlur('first_name')}
                val={formik.values.first_name}
              />
              <div className='error'>{formik.touched.first_name && formik.errors.first_name}</div>
            </div>
            <div className='w-50'>
              <CustomInput
                type='text'
                label='Enter Last Name'
                name='last_name'
                onChng={formik.handleChange('last_name')}
                onBlr={formik.handleBlur('last_name')}
                val={formik.values.last_name}
              />
              <div className='error'>{formik.touched.last_name && formik.errors.last_name}</div>
            </div>
          </div>

          <div className='d-flex gap-4'>
            <div className='w-50'>
              <CustomInput
                type='text'
                label='Enter Username'
                name='username'
                onChng={formik.handleChange('username')}
                onBlr={formik.handleBlur('username')}
                val={formik.values.username}
              />
              <div className='error'>{formik.touched.username && formik.errors.username}</div>
            </div>
            <div className='w-50'>
              <CustomInput
                type='text'
                label='Enter Password'
                name='password'
                onChng={formik.handleChange('password')}
                onBlr={formik.handleBlur('password')}
                val={formik.values.password}
              />
              <div className='error'>{formik.touched.password && formik.errors.password}</div>
            </div>
          </div>
          <CustomInput
            type='email'
            label='Enter Email'
            name='email'
            onChng={formik.handleChange('email')}
            onBlr={formik.handleBlur('email')}
            val={formik.values.email}
          />
          <div className='error'>{formik.touched.email && formik.errors.email}</div>

          <div className='d-flex gap-4 align-items-center'>
            <div className='flex-fill'>
              <CustomInput
                type='date'
                label='Enter Date Of Birth'
                name='date_of_birth'
                onChng={formik.handleChange('date_of_birth')}
                onBlr={formik.handleBlur('date_of_birth')}
                val={formik.values.date_of_birth}
              />
              <div className='error'>{formik.touched.date_of_birth && formik.errors.date_of_birth}</div>
            </div>
            <div className='flex-fill'>
              <CustomInput
                type='text'
                label='Enter telephone'
                name='telephone'
                onChng={formik.handleChange('telephoneNumber')}
                onBlr={formik.handleBlur('telephoneNumber')}
                val={formik.values.telephoneNumber}
              />
              <div className='error'>{formik.touched.telephoneNumber && formik.errors.telephoneNumber}</div>
            </div>
            <div className='flex-fill'>
              <select
                value={formik.values.role}
                onChange={formik.handleChange('role')}
                className='form-control py-3'
                id=''
              >
                <option value='NORMAL_USER'>Normal User</option>
                <option value='ADMIN'>Admin</option>
              </select>
            </div>
          </div>

          <div>Addres</div>
          <CustomInput
            type='text'
            label='Enter Home Number'
            name='home_number'
            onChng={formik.handleChange('home_number')}
            onBlr={formik.handleBlur('home_number')}
            val={formik.values.home_number}
          />
          <div className='error'>{formik.touched.home_number && formik.errors.home_number}</div>
          <div className='d-flex'>
            <div className='flex-fill'>
              <CustomInput
                type='text'
                label='Enter Street'
                name='street'
                onChng={formik.handleChange('street')}
                onBlr={formik.handleBlur('street')}
                val={formik.values.street}
              />
              <div className='error'>{formik.touched.street && formik.errors.street}</div>
            </div>
            <div className='flex-fill'>
              <CustomInput
                type='text'
                label='Enter State'
                name='state'
                onChng={formik.handleChange('state')}
                onBlr={formik.handleBlur('state')}
                val={formik.values.state}
              />
              <div className='error'>{formik.touched.state && formik.errors.state}</div>
            </div>
            <div className='flex-fill'>
              <CustomInput
                type='text'
                label='Enter City'
                name='city'
                onChng={formik.handleChange('city')}
                onBlr={formik.handleBlur('city')}
                val={formik.values.city}
              />
              <div className='error'>{formik.touched.city && formik.errors.city}</div>
            </div>
          </div>

          <div className='showimages d-flex flex-wrap gap-3'></div>
          <button className='btn btn-success border-0 rounded-3 my-5' type='submit'>
            Add New Customer
          </button>
        </form>
      </div>
    </div>
  )
}

export default AddCustomer
