import React, { useEffect, useState } from 'react'
import { Table, Tag } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { FaPlus } from 'react-icons/fa'
import { BiEdit } from 'react-icons/bi'
import { AiFillDelete } from 'react-icons/ai'
import { Link, useLocation } from 'react-router-dom'
import CustomModal from '../../components/CustomModal'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { ExportCSVButton } from '../../components/ExportCSVButton'
import { useNavigate } from 'react-router-dom'
import { Select } from 'antd'
import { deleteItemsMethod, deleteProductMethod, getListItemsMethod, getListProductMethod } from '../../features/product/middlewate'
import { IoIosStar } from 'react-icons/io'
const columns = [
  {
    title: 'SNo',
    dataIndex: 'id',
    width: 60,
    fixed: 'left'
  },
  {
    title: 'Image',
    dataIndex: 'image',
    width: 120,
    fixed: 'left'
  },
  {
    title: 'Title',
    dataIndex: 'title',
    width: 200,
    fixed: 'left',
    sorter: (a, b) => a.title.localeCompare(b.title)
  },
  {
    title: 'Status',
    dataIndex: 'status',
    sorter: (a, b) => a.status.localeCompare(b.status),
    render: (status) => {
      let color = status === 'On Stock' ? 'blue' : 'red'
      return <Tag color={color}>{status}</Tag>
    }
  },
  {
    title: 'Price',
    dataIndex: 'price',
    sorter: (a, b) => a.price - b.price,
    render: (price) => {
      return <span>{price?.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}</span>
    }
  },
  {
    title: 'Rating',
    dataIndex: 'averageRating',
    sorter: (a, b) => a.averageRating - b.averageRating,
    render: (averageRating) => {
      let color = averageRating >= 4 ? 'green' : averageRating >= 2 ? 'orange' : 'red'
      return (
        <Tag className='d-flex align-items-center gap-1 justify-content-center' color={color}>
          {averageRating} <IoIosStar color={color} className='fs-6' />
        </Tag>
      )
    }
  },
  {
    title: 'Quantity Sold',
    dataIndex: 'quantity_sold',
    sorter: (a, b) => a.quantity_sold - b.quantity_sold
  },
  {
    title: 'Inventory',
    dataIndex: 'inventory',
    sorter: (a, b) => a.inventory - b.inventory
  },
  {
    title: 'Category',
    dataIndex: 'category',
    sorter: (a, b) => a.category.localeCompare(b.category)
  },
  {
    title: 'Collection',
    dataIndex: 'collection',
    sorter: (a, b) => a.collection.localeCompare(b.collection)
  },
  {
    title: 'Brand',
    dataIndex: 'brand',
    sorter: (a, b) => a.brand.localeCompare(b.brand)
  },
  {
    title: 'Discount',
    dataIndex: 'discount',
    sorter: (a, b) => a.discount - b.discount,
    render: (discount) => {
      let discountColor = discount >= 10 ? 'red' : discount >= 5 ? 'orange' : 'green'
      return <Tag color={discountColor}>{discount ? `${discount}%` : 'No discount'}</Tag>
    }
  },
  {
    title: 'Action',
    dataIndex: 'action',
    fixed: 'right',
    width: 110
  }
]

// {
//   "id": 1,
//   "title": "iPhone 15 Pro Max 256GB | Chính hãng VN/A",
//   "status": "On Stock",
//   "averageRating": "3.4",
//   "price": 34990000,
//   "quantity_sold": 15,
//   "colection": {
//       "id": 1,
//       "name": "Iphone 15 Series",
//       "thumbnail": "https://firebasestorage.googleapis.com/v0/b/ecommerce-image-a4ac6.appspot.com/o/avatar%2F9c050a4d-d417-47e3-bded-2fc2b3ec1f31_Class%20Diagram%20-%20ERD.png_03012024090921?alt=media&token=1505fe6c-e588-4bf0-a03e-bf11ca9519d9"
//   },
//   "brand": {
//       "id": 1,
//       "name": "Apple",
//       "thumbnail": "https://media.licdn.com/dms/image/D4D12AQHwi4jdRd3fQQ/article-cover_image-shrink_600_2000/0/1685279753620?e=2147483647&v=beta&t=7I-pJ0kDQfNl4w-0Ue8aPyol_X-aWOQlzp18NhTldys"
//   },
//   "category": {
//       "id": 1,
//       "name": "Mobile Phone",
//       "thumbnail": "https://cdn2.cellphones.com.vn/insecure/rs:fill:0:358/q:80/plain/https://cellphones.com.vn/media/catalog/product/s/m/sm-g990_s21fe_backfront_lv.png"
//   },
//   "discount": {
//       "discount_percent": "6.0",
//       "status": false,
//       "effective_time": "2023-12-20T23:59",
//       "product": 1
//   },
//   "image": "https://cdn2.cellphones.com.vn/insecure/rs:fill:358:358/q:80/plain/https://cellphones.com.vn/media/catalog/product/i/p/iphone-15-pro-max_3.png",
//   "inventory": 546
// }

const Productlist = () => {
  const { pathname } = useLocation()
  const dispatch = useDispatch()
  const nav = useNavigate()
  const [listItem, setListItem] = useState([])
  const itemState = useSelector((state) => state.product.listItems)
  const [modalOpen, setModalOpen] = useState(false)
  const [item, setItem] = useState('') // save id of item for remove
  const [searchKey, setSearchKey] = useState('') //save search key
  const [searchType, setSearchType] = useState('title') //save search type

  useEffect(() => {
    dispatch(getListProductMethod())
  }, [])

  useEffect(() => {
    const newArray = itemState.map((item, index) => {
      return {
        id: index + 1,
        image: <img src={item.image} alt='' style={{ width: 100, height: 50 }} />,
        title: item.title,
        status: item.status,
        price: item.price,
        averageRating: item.averageRating,
        quantity_sold: item.quantity_sold,
        inventory: item.inventory,
        category: item?.category?.name,
        collection: item?.colection?.name,
        brand: item?.brand?.name,
        discount: item?.discount?.discount_percent,
        action: (
          <>
            <Link to={`${item.id}`} className=' fs-3 text-danger'>
              <BiEdit color='#0b7ba7' />
            </Link>
            <span className='ms-3 fs-3 text-danger' onClick={() => showModal(item.id)}>
              <AiFillDelete />
            </span>
          </>
        )
      }
    })
    setListItem(newArray)
  }, [itemState])

  useEffect(() => {
    if (searchKey !== '') {
      const newArray = itemState
        .filter((item) => {
          return item.title.toLowerCase().includes(searchKey.toLowerCase())
        })
        .map((item, index) => {
          return {
            id: index + 1,
            image: <img src={item.image} alt='' style={{ width: 100, height: 50 }} />,
            title: item.title,
            status: item.status,
            price: item.price,
            averageRating: item.averageRating,
            quantity_sold: item.quantity_sold,
            inventory: item.inventory,
            category: item?.category?.name,
            collection: item?.colection?.name,
            brand: item?.brand?.name,
            discount: item?.discount?.discount_percent,
            action: (
              <>
                <Link to={`${item.id}`} className=' fs-3 text-danger'>
                  <BiEdit color='#0b7ba7' />
                </Link>
                <span className='ms-3 fs-3 text-danger' onClick={() => showModal(item.id)}>
                  <AiFillDelete />
                </span>
              </>
            )
          }
        })
      setListItem(newArray)
    } else {
      const newArray = itemState.map((item, index) => {
        return {
          id: index + 1,
          image: <img src={item.image} alt='' style={{ width: 100, height: 50 }} />,
          title: item.title,
          status: item.status,
          price: item.price,
          averageRating: item.averageRating,
          quantity_sold: item.quantity_sold,
          inventory: item.inventory,
          category: item?.category?.name,
          collection: item?.colection?.name,
          brand: item?.brand?.name,
          discount: item?.discount?.discount_percent,
          action: (
            <>
              <Link to={`${item.id}`} className=' fs-3 text-danger'>
                <BiEdit color='#0b7ba7' />
              </Link>
              <span className='ms-3 fs-3 text-danger' onClick={() => showModal(item.id)}>
                <AiFillDelete />
              </span>
            </>
          )
        }
      })
      setListItem(newArray)
    }
  }, [searchKey, searchType])

  const showModal = (id) => {
    setModalOpen(true)
    setItem(id)
  }

  const handleDeleteProduct = async (id) => {
    dispatch(deleteProductMethod(id))
      .then(() => {
        setModalOpen(false)
        return dispatch(getListProductMethod())
      })
      .then(() => {
        toast.success('Delete product successful', {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          theme: 'light'
        })
      })
      .catch((error) => {
        toast.error('Delete product failed', {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          theme: 'light'
        })
      })
  }

  return (
    <div>
      <ToastContainer />
      <CustomModal
        open={modalOpen}
        hideModal={() => setModalOpen(false)}
        performAction={() => handleDeleteProduct(item)}
        title={`Are you sure you want to delete this item?`}
      />
      <h3 className='mb-4 title'>
        List {pathname === '/admin/brands' ? 'brand' : pathname === '/admin/categories' ? 'category' : 'collection'}
      </h3>
      <div
        className='d-flex justify-content-between mb-4 p-2 box-shadown'
        style={{ background: 'white', borderRadius: '10px' }}
      >
        <div className='d-flex gap-3'>
          <input
            type='text'
            className='form-control'
            placeholder='Search...'
            value={searchKey}
            onChange={(e) => setSearchKey(e.target.value)}
          />
          <Select
            value={searchType}
            onChange={(e) => setSearchType(e)}
            style={{ width: 240, height: '100%' }}
            options={[{ value: 'title', label: 'Title' }]}
          />
        </div>
        <div>
          <button className='btn btn-info fw-bold text-white' onClick={() => nav('add')}>
            <FaPlus /> Add Product
          </button>
          <ExportCSVButton csvData={listItem} fileName='product' />
        </div>
      </div>
      <div className='box-shadown rounded-3'>
        {listItem.length > 0 ? (
          <Table
            columns={columns}
            dataSource={listItem}
            pagination={{
              defaultPageSize: 5
            }}
            scroll={{
              x: 1500
            }}
          />
        ) : (
          <div className='text-center fw-bold fs-6'>No have data</div>
        )}
      </div>
    </div>
  )
}

export default Productlist
