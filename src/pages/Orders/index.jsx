import React, { useEffect } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { BiEdit } from "react-icons/bi";
import { AiFillDelete } from "react-icons/ai";
import { Link } from "react-router-dom";
// import { getOrders } from "../features/auth/authSlice";
const columns = [
  {
    title: "SNo",
    dataIndex: "key",
  },
  {
    title: "Name",
    dataIndex: "name",
  },
  {
    title: "Detail",
    dataIndex: "detail",
  },
  {
    title: "Amount",
    dataIndex: "amount",
  },
  {
    title: "Date",
    dataIndex: "date",
  },

  {
    title: "Action",
    dataIndex: "action",
  },
];

const Orders = () => {
  // const dispatch = useDispatch();
  // useEffect(() => {
  //   dispatch(getOrders());
  // }, []);
  // const orderState = useSelector((state) => state.auth.orders);

  const data1 = [];
  for (let i = 0; i < 10; i++) {
    data1.push({
      key: i + 1,
      name: `#${i+4}`,
      detail: (
        <Link to={`1`}>
          View Orders
        </Link>
      ),
      amount: 15200000,
      date: "12-12-2023",
      action: (
        <>
          <Link to="/" className=" fs-3 text-danger">
            <BiEdit color="#0b7ba7" />
          </Link>
          <Link className="ms-3 fs-3 text-danger" to="/">
            <AiFillDelete />
          </Link>
        </>
      ),
    });
  }
  return (
    <div>
      <h3 className="mb-4 title">Orders</h3>
      <div>{<Table columns={columns} dataSource={data1} />}</div>
    </div>
  );
};

export default Orders;
