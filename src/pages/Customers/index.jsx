import React, { useEffect, useState } from 'react'
import { Table, Modal } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { FaPlus } from 'react-icons/fa'
import { deleteCustomerMethod, getAllCustomerMethod } from '../../features/customer/middlewate'
import { BiEdit } from 'react-icons/bi'
import { AiFillDelete } from 'react-icons/ai'
import { Link } from 'react-router-dom'
import CustomModal from '../../components/CustomModal'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { ExportCSVButton } from '../../components/ExportCSVButton'
import { useNavigate } from 'react-router-dom'
import { Select } from 'antd'
const columns = [
  {
    title: 'SNo',
    dataIndex: 'id'
  },
  {
    title: 'Name',
    dataIndex: 'name',
    sorter: (a, b) => a.name.localeCompare(b.name)
  },
  {
    title: 'Username',
    dataIndex: 'username',
    sorter: (a, b) => a.name.localeCompare(b.name)
  },
  {
    title: 'Email',
    dataIndex: 'email',
    sorter: (a, b) => a.name.localeCompare(b.name)
  },
  {
    title: 'Phone Number',
    dataIndex: 'telephoneNumber'
  },
  {
    title: 'Date of Birth',
    dataIndex: 'date_of_birth'
  },
  {
    title: 'Action',
    dataIndex: 'action'
  }
]

const Customers = () => {
  const dispatch = useDispatch()
  const nav = useNavigate()
  const [listCustomer, setListCustomer] = useState([])
  const customerState = useSelector((state) => state.customer.listCustomer)
  const [modalOpen, setModalOpen] = useState(false)
  const [customer, setCustomer] = useState('') // save username of customer for remove
  const [searchKey, setSearchKey] = useState('') //save search key
  const [searchType, setSearchType] = useState('Name') //save search type

  useEffect(() => {
    dispatch(getAllCustomerMethod())
  }, [])

  useEffect(() => {
    const newArray = customerState.map((item, index) => {
      return {
        id: index + 1,
        name: `${item.first_name} ${item.last_name}`,
        username: item.username,
        email: item.email,
        telephoneNumber: item.telephoneNumber ? item.telephoneNumber : 'N/A',
        date_of_birth: item.date_of_birth ? item.date_of_birth : 'N/A',
        action: (
          <>
            <Link to={item.username} className=' fs-3 text-danger'>
              <BiEdit color='#0b7ba7' />
            </Link>
            <span className='ms-3 fs-3 text-danger' onClick={() => showModal(item.username)}>
              <AiFillDelete />
            </span>
          </>
        )
      }
    })
    setListCustomer(newArray)
  }, [customerState])

  useEffect(() => {
    if (searchKey !== '') {
      const newArray = customerState
        .filter((item) => {
          if (searchType === 'Name') {
            return (item.first_name+" "+item.last_name).toLowerCase().includes(searchKey.toLowerCase())
          } else if (searchType === 'Usename') {
            return item.username.toLowerCase().includes(searchKey.toLowerCase())
          } else {
            return item.email.toLowerCase().includes(searchKey.toLowerCase())
          }
        })
        .map((item, index) => {
          return {
            id: index + 1,
            name: `${item.first_name} ${item.last_name}`,
            username: item.username,
            email: item.email,
            telephoneNumber: item.telephoneNumber ? item.telephoneNumber : 'N/A',
            date_of_birth: item.date_of_birth ? item.date_of_birth : 'N/A',
            action: (
              <>
                <Link to={item.username} className=' fs-3 text-danger'>
                  <BiEdit color='#0b7ba7' />
                </Link>
                <span className='ms-3 fs-3 text-danger' onClick={() => showModal(item.username)}>
                  <AiFillDelete />
                </span>
              </>
            )
          }
        })
      setListCustomer(newArray)
    } else{
      const newArray = customerState.map((item, index) => {
        return {
          id: index + 1,
          name: `${item.first_name} ${item.last_name}`,
          username: item.username,
          email: item.email,
          telephoneNumber: item.telephoneNumber ? item.telephoneNumber : 'N/A',
          date_of_birth: item.date_of_birth ? item.date_of_birth : 'N/A',
          action: (
            <>
              <Link to={item.username} className=' fs-3 text-danger'>
                <BiEdit color='#0b7ba7' />
              </Link>
              <span className='ms-3 fs-3 text-danger' onClick={() => showModal(item.username)}>
                <AiFillDelete />
              </span>
            </>
          )
        }
      })
      setListCustomer(newArray)
    }
  }, [searchKey, searchType])

  const showModal = (username) => {
    setModalOpen(true)
    setCustomer(username)
  }

  const handleDeleteCustomer = async (username) => {
    dispatch(deleteCustomerMethod(username))
      .then(() => {
        setModalOpen(false)
        return dispatch(getAllCustomerMethod())
      })
      .then(() => {
        toast.success('Delete customer successful', {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          theme: 'light'
        })
      })
      .catch((error) => {
        toast.error('Delete customer faled', {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          theme: 'light'
        })
      })
  }

  return (
    <div>
      <ToastContainer />
      <CustomModal
        open={modalOpen}
        hideModal={() => setModalOpen(false)}
        performAction={() => handleDeleteCustomer(customer)}
        title={`Are you sure you want to delete ${customer}?`}
      />
      <h3 className='mb-4 title'>Customers</h3>
      <div
        className='d-flex justify-content-between mb-4 p-2 box-shadown'
        style={{ background: 'white', borderRadius: '10px' }}
      >
        <div className='d-flex gap-3'>
          <input
            type='text'
            className='form-control'
            placeholder='Search...'
            value={searchKey}
            onChange={(e) => setSearchKey(e.target.value)}
          />
          <Select
            value={searchType}
            onChange={(e) => setSearchType(e)}
            style={{ width: 240, height: '100%' }}
            options={[
              { value: 'Name', label: 'Name' },
              { value: 'Usename', label: 'Username' },
              { value: 'Email', label: 'Email' }
            ]}
          />
        </div>
        <div>
          <button className='btn btn-info fw-bold text-white' onClick={() => nav('add')}>
            <FaPlus /> Add Customer
          </button>
          <ExportCSVButton csvData={listCustomer} fileName='customers' />
        </div>
      </div>
      <div className='box-shadown rounded-3'>
        {listCustomer.length > 0 ? (
          <Table
            columns={columns}
            dataSource={listCustomer}
            pagination={{
              defaultPageSize: 5
            }}
          />
        ) : (
          <div className='text-center fw-bold fs-6'>No have data</div>
        )}
      </div>
    </div>
  )
}

export default Customers
