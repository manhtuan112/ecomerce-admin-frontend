import React, { useEffect, useState } from 'react'
import { Table, Modal } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { FaPlus } from 'react-icons/fa'
import { BiEdit } from 'react-icons/bi'
import { AiFillDelete } from 'react-icons/ai'
import { Link, useLocation } from 'react-router-dom'
import CustomModal from '../../components/CustomModal'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { ExportCSVButton } from '../../components/ExportCSVButton'
import { useNavigate } from 'react-router-dom'
import { Select } from 'antd'
import { deleteItemsMethod, getListItemsMethod } from '../../features/product/middlewate'
import { setTypeofItems } from '../../features/product/productSlice'
const columns = [
  {
    title: 'SNo',
    dataIndex: 'id'
  },
  {
    title: 'Image',
    dataIndex: 'thumbnail'
  },
  {
    title: 'Name',
    dataIndex: 'name',
    sorter: (a, b) => a.name.localeCompare(b.name)
  },
  {
    title: 'Action',
    dataIndex: 'action'
  }
]

const Brandlist = () => {
  const { pathname } = useLocation()
  const dispatch = useDispatch()
  const nav = useNavigate()
  const [listItem, setListItem] = useState([])
  const itemState = useSelector((state) => state.product.listItems)
  const [modalOpen, setModalOpen] = useState(false)
  const [item, setItem] = useState('') // save id of item for remove
  const [searchKey, setSearchKey] = useState('') //save search key
  const [searchType, setSearchType] = useState('Name') //save search type

  useEffect(() => {
    if (pathname === '/admin/brands') {
      dispatch(getListItemsMethod({ type: 'brand' }))
      dispatch(setTypeofItems('brand'))
    } else if (pathname === '/admin/categories') {
      dispatch(getListItemsMethod({ type: 'category' }))
      dispatch(setTypeofItems('category'))
    } else if (pathname === '/admin/collections') {
      dispatch(getListItemsMethod({ type: 'collection' }))
      dispatch(setTypeofItems('collection'))
    }
  }, [pathname])

  useEffect(() => {
    const newArray = itemState.map((item, index) => {
      return {
        key: index,
        id: index + 1,
        name: item.name,
        thumbnail: <img src={item.thumbnail} alt='' style={{ width: 100, height: 50 }} />,
        action: (
          <>
            <Link to={`${item.id}`} className=' fs-3 text-danger'>
              <BiEdit color='#0b7ba7' />
            </Link>
            <span className='ms-3 fs-3 text-danger' onClick={() => showModal(item.id)}>
              <AiFillDelete />
            </span>
          </>
        )
      }
    })
    setListItem(newArray)
  }, [itemState])

  useEffect(() => {
    if (searchKey !== '') {
      const newArray = itemState
        .filter((item) => {
          return item.name.toLowerCase().includes(searchKey.toLowerCase())
        })
        .map((item, index) => {
          return {
            id: index + 1,
            name: item.name,
            thumbnail: <img src={item.thumbnail} alt='' style={{ width: 100, height: 50 }} />,
            action: (
              <>
                <Link to={item.id} className=' fs-3 text-danger'>
                  <BiEdit color='#0b7ba7' />
                </Link>
                <span className='ms-3 fs-3 text-danger' onClick={() => showModal(item.id)}>
                  <AiFillDelete />
                </span>
              </>
            )
          }
        })
      setListItem(newArray)
    } else {
      const newArray = itemState.map((item, index) => {
        return {
          key: index,
          id: index + 1,
          name: item.name,
          thumbnail: <img src={item.thumbnail} alt='' style={{ width: 100, height: 50 }} />,
          action: (
            <>
              <Link to={item.id} className=' fs-3 text-danger'>
                <BiEdit color='#0b7ba7' />
              </Link>
              <span className='ms-3 fs-3 text-danger' onClick={() => showModal(item.id)}>
                <AiFillDelete />
              </span>
            </>
          )
        }
      })
      setListItem(newArray)
    }
  }, [searchKey, searchType])

  const showModal = (id) => {
    setModalOpen(true)
    setItem(id)
  }

  const handleDeleteItem = async (id) => {
    let type = ''
    if (pathname === '/admin/brands') {
      type = 'brand'
    } else if (pathname === '/admin/categories') {
      type = 'category'
    } else if (pathname === '/admin/collections') {
      type = 'collection'
    }
    dispatch(deleteItemsMethod({ type: type, id }))
      .then(() => {
        setModalOpen(false)
        return dispatch(getListItemsMethod({type: type}))
      })
      .then(() => {
        toast.success('Delete item successful', {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          theme: 'light'
        })
      })
      .catch((error) => {
        toast.error('Delete item faled', {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          theme: 'light'
        })
      })
  }

  return (
    <div>
      <ToastContainer />
      <CustomModal
        open={modalOpen}
        hideModal={() => setModalOpen(false)}
        performAction={() => handleDeleteItem(item)}
        title={`Are you sure you want to delete this item?`}
      />
      <h3 className='mb-4 title'>List {pathname === '/admin/brands' ? 'brand' : (pathname === '/admin/categories' ? 'category':'collection')}</h3>
      <div
        className='d-flex justify-content-between mb-4 p-2 box-shadown'
        style={{ background: 'white', borderRadius: '10px' }}
      >
        <div className='d-flex gap-3'>
          <input
            type='text'
            className='form-control'
            placeholder='Search...'
            value={searchKey}
            onChange={(e) => setSearchKey(e.target.value)}
          />
          <Select
            value={searchType}
            onChange={(e) => setSearchType(e)}
            style={{ width: 240, height: '100%' }}
            options={[{ value: 'Name', label: 'Name' }]}
          />
        </div>
        <div>
          <button className='btn btn-info fw-bold text-white' onClick={() => nav('add')}>
            <FaPlus /> Add {pathname === '/admin/brands' ? 'brand' : (pathname === '/admin/categories' ? 'category':'collection')}
          </button>
          <ExportCSVButton csvData={listItem} fileName={pathname === '/admin/brands' ? 'brands' : (pathname === '/admin/categories' ? 'categories':'collections')} />
        </div>
      </div>
      <div className='box-shadown rounded-3'>
        {listItem.length > 0 ? (
          <Table
            columns={columns}
            dataSource={listItem}
            pagination={{
              defaultPageSize: 4
            }}
          />
        ) : (
          <div className='text-center fw-bold fs-6'>No have data</div>
        )}
      </div>
    </div>
  )
}

export default Brandlist
