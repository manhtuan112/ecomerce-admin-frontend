import { React, useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import 'react-quill/dist/quill.snow.css'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import * as yup from 'yup'
import { useFormik } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import './style.scss'
import CustomInput from '../../components/CustomInput'
import { storage } from '../../../firebaseConfig'
import { getDownloadURL, ref, uploadBytes } from 'firebase/storage'
import { v4 } from 'uuid'
import { getCurrentDateTimeFormatted } from '../../features/common'
import {
  addCustomerMethod,
  getCustomerDetailMethod,
  updateCustomerDetailMethod
} from '../../features/customer/middlewate'
import Loading from '../loading'
import { unwrapResult } from '@reduxjs/toolkit'
let schema = yup.object().shape({
  username: yup.string().required('Title is Required'),
  email: yup.string().required('Email is Required'),
  last_name: yup.string().required('LastName is Required'),
  first_name: yup.string().required('FisrtName is Required'),
  date_of_birth: yup.string().required('Date of Birth is Required'),
  telephoneNumber: yup.string().required('Telephone is Required'),
  role: yup.string().required('Role is Required'),
  street: yup.string().required('Street is Required'),
  city: yup.string().required('City is Required'),
  state: yup.string().required('State is Required'),
  home_number: yup.string().required('Home Number is Required')
})

const UpdateCustomer = () => {
  const { username } = useParams()
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const [selectedImage, setSelectedImage] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const [avatar, setAvatar] = useState('')
  const { isError, ErrorMessage } = useSelector((state) => state.customer)

  useEffect(() => {
    const fetchData = async () => {
      const data = await dispatch(getCustomerDetailMethod(username))
      setAvatar(data.payload.data.avatar)
      formik.setFieldValue('username', data.payload.data.username)
      formik.setFieldValue('email', data.payload.data.email)
      formik.setFieldValue('last_name', data.payload.data.last_name)
      formik.setFieldValue('first_name', data.payload.data.first_name)
      formik.setFieldValue('date_of_birth', data.payload.data.date_of_birth ? data.payload.data.date_of_birth : '')
      formik.setFieldValue('telephoneNumber', data.payload.data.telephoneNumber)
      formik.setFieldValue('role', data.payload.data.role)
      formik.setFieldValue('street', data.payload.data.address?.street ? data.payload.data.address?.street : '')
      formik.setFieldValue('city', data.payload.data.address?.city ? data.payload.data.address?.city : '')
      formik.setFieldValue('state', data.payload.data.address?.state ? data.payload.data.address?.state : '')
      formik.setFieldValue(
        'home_number',
        data.payload.data.address?.home_number ? data.payload.data.address?.home_number : ''
      )
    }
    fetchData()
  }, [])

  const uploadImage = () => {
    return new Promise((resolve, reject) => {
      if (selectedImage === null) return reject('No image selected')
      const imageRef = ref(storage, `avatar/${v4() + '_' + selectedImage.name + '_' + getCurrentDateTimeFormatted()}`)

      uploadBytes(imageRef, selectedImage)
        .then((res) => {
          getDownloadURL(res.ref)
            .then((url) => {
              resolve(url)
            })
            .catch((e) => {
              reject(e)
            })
        })
        .catch((e) => {
          reject(e)
        })
    })
  }

  const formik = useFormik({
    initialValues: {
      username: '',
      email: '',
      last_name: '',
      first_name: '',
      password: '',
      date_of_birth: '',
      telephoneNumber: '',
      role: 'NORMAL_USER',
      street: '',
      city: '',
      state: '',
      home_number: ''
    },
    validationSchema: schema,
    onSubmit: (values) => {
      handleUpdateCustomer()
    }
  })

  const handleUpdateCustomer = async () => {
    setIsLoading(true)
    let url = null
    if (selectedImage) {
      // neu co chon anh moi
      try {
        url = await uploadImage()
      } catch (error) {
        console.log(error)
      }
    }
    try {
      const data = {
        username: formik.values.username,
        email: formik.values.email,
        last_name: formik.values.last_name,
        first_name: formik.values.first_name,
        date_of_birth: formik.values.date_of_birth,
        telephoneNumber: formik.values.telephoneNumber,
        role: formik.values.role,
        address: {
          street: formik.values.street,
          city: formik.values.city,
          state: formik.values.state,
          home_number: formik.values.home_number
        },
        avatar: url ? url : avatar
      }
      const resultAction = await dispatch(updateCustomerDetailMethod({ username: username, data: data }))
      console.log(resultAction)
      if ((resultAction.type === 'customer/updateCustomerDetailMethod/rejected')) {
        toast.error(`Update customer failed: ${resultAction.payload}`, {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          theme: 'light'
        })
        setIsLoading(false)
      } else {
        toast.success('Update customer successful', {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          theme: 'light'
        })
        setIsLoading(false)

        setTimeout(() => {
          navigate('/admin/customers')
        }, 1000)
      }
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div>
      <ToastContainer />
      {isLoading && <Loading />}
      <h3 className='mb-4 username'>Update Customer</h3>
      <div>
        <img
          className='img-account-profile rounded-circle mb-2 img-fluid'
          src={
            selectedImage
              ? URL.createObjectURL(selectedImage)
              : avatar
                ? avatar
                : 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png'
          }
          alt='image'
        />
        <button className='btn btn-primary' type='button'>
          <label htmlFor='upload image'>Upload new image</label>
        </button>
        <input
          type='file'
          name='myImage'
          id='upload image'
          className='d-none'
          onChange={(event) => {
            setSelectedImage(event.target.files[0])
          }}
        />
        <form onSubmit={formik.handleSubmit} className='d-flex gap-3 flex-column'>
          <div className='d-flex gap-5'>
            <div className='w-50'>
              <CustomInput
                type='text'
                label='Enter First Name'
                name='first_name'
                onChng={formik.handleChange('first_name')}
                onBlr={formik.handleBlur('first_name')}
                val={formik.values.first_name}
              />
              <div className='error'>{formik.touched.first_name && formik.errors.first_name}</div>
            </div>
            <div className='w-50'>
              <CustomInput
                type='text'
                label='Enter Last Name'
                name='last_name'
                onChng={formik.handleChange('last_name')}
                onBlr={formik.handleBlur('last_name')}
                val={formik.values.last_name}
              />
              <div className='error'>{formik.touched.last_name && formik.errors.last_name}</div>
            </div>
          </div>

          <div className='d-flex gap-4'>
            <div className='w-50'>
              <CustomInput
                type='text'
                label='Enter Username'
                name='username'
                onChng={formik.handleChange('username')}
                onBlr={formik.handleBlur('username')}
                val={formik.values.username}
              />
              <div className='error'>{formik.touched.username && formik.errors.username}</div>
            </div>
            <div className='flex-fill'>
              <CustomInput
                type='text'
                label='Enter telephone'
                name='telephone'
                onChng={formik.handleChange('telephoneNumber')}
                onBlr={formik.handleBlur('telephoneNumber')}
                val={formik.values.telephoneNumber}
              />
              <div className='error'>{formik.touched.telephoneNumber && formik.errors.telephoneNumber}</div>
            </div>
          </div>
          <CustomInput
            type='email'
            label='Enter Email'
            name='email'
            onChng={formik.handleChange('email')}
            onBlr={formik.handleBlur('email')}
            val={formik.values.email}
          />
          <div className='error'>{formik.touched.email && formik.errors.email}</div>

          <div className='d-flex gap-4 align-items-start'>
            <div className='flex-fill'>
              <CustomInput
                type='date'
                label='Enter Date Of Birth'
                name='date_of_birth'
                onChng={formik.handleChange('date_of_birth')}
                onBlr={formik.handleBlur('date_of_birth')}
                val={formik.values.date_of_birth}
              />
              <div className='error'>{formik.touched.date_of_birth && formik.errors.date_of_birth}</div>
            </div>
            <div className='flex-fill mt-3'>
              <select
                value={formik.values.role}
                onChange={formik.handleChange('role')}
                className='form-control py-3'
                id=''
              >
                <option value='NORMAL_USER'>Normal User</option>
                <option value='ADMIN'>Admin</option>
              </select>
            </div>
          </div>

          <h5 className='mb-0'>Addres</h5>
          <CustomInput
            type='text'
            label='Enter Home Number'
            name='home_number'
            onChng={formik.handleChange('home_number')}
            onBlr={formik.handleBlur('home_number')}
            val={formik.values.home_number}
          />
          <div className='error'>{formik.touched.home_number && formik.errors.home_number}</div>
          <div className='d-flex'>
            <div className='flex-fill'>
              <CustomInput
                type='text'
                label='Enter Street'
                name='street'
                onChng={formik.handleChange('street')}
                onBlr={formik.handleBlur('street')}
                val={formik.values.street}
              />
              <div className='error'>{formik.touched.street && formik.errors.street}</div>
            </div>
            <div className='flex-fill'>
              <CustomInput
                type='text'
                label='Enter State'
                name='state'
                onChng={formik.handleChange('state')}
                onBlr={formik.handleBlur('state')}
                val={formik.values.state}
              />
              <div className='error'>{formik.touched.state && formik.errors.state}</div>
            </div>
            <div className='flex-fill'>
              <CustomInput
                type='text'
                label='Enter City'
                name='city'
                onChng={formik.handleChange('city')}
                onBlr={formik.handleBlur('city')}
                val={formik.values.city}
              />
              <div className='error'>{formik.touched.city && formik.errors.city}</div>
            </div>
          </div>

          <div className='showimages d-flex flex-wrap gap-3'></div>
          <div className='d-flex gap-3 justify-content-center'>
            <button className='btn btn-primary' type='submit'>
              Update Customer
            </button>
            <button
              className='btn btn-secondary'
              type='button'
              onClick={() => {
                navigate('/admin/customers')
              }}
            >
              Back
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default UpdateCustomer
