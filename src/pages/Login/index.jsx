import React, { useEffect } from 'react'
import './style.scss'
import CustomInput from '../../components/CustomInput'
import { Link, useNavigate } from 'react-router-dom'
import { useFormik } from 'formik'
import * as yup from 'yup'
import { useDispatch, useSelector } from 'react-redux'
import { loginMethod } from '../../features/authenticate/middlewate'
function Login() {
  const dispatch = useDispatch()
  const nav = useNavigate()
  let schema = yup.object().shape({
    username: yup.string().required('Username is Required'),
    password: yup.string().required('Password is Required')
  })
  const formik = useFormik({
    initialValues: {
      username: '',
      password: ''
    },
    validationSchema: schema,
    onSubmit: (values) => {
      dispatch(loginMethod(values))
    }
  })
  const { user, isLoading, isError, isSuccess, message } = useSelector((state) => state.user)
  useEffect(() => {
    if (isSuccess) {
      nav('/admin')
    }
  }, [isLoading])

  return (
    <div className='login-wrapper py-5'>
      <br />
      <br />
      <div className='my-5 w-25 bg-white rounded-3 mx-auto p-4'>
        <h3 className='text-center title'>Login</h3>
        <p className='text-center'>Login to your account to continue.</p>
        <div className='error text-center'>{message == 'Rejected' ? 'You are not an Admin' : ''}</div>
        <form action='' onSubmit={formik.handleSubmit}>
          <CustomInput
            type='text'
            label='Username'
            id='username'
            name='username'
            onChng={formik.handleChange('username')}
            onBlr={formik.handleBlur('username')}
            val={formik.values.username}
          />
          <div className='error mt-2'>{formik.touched.username && formik.errors.username}</div>
          <CustomInput
            type='password'
            label='Password'
            id='pass'
            name='password'
            onChng={formik.handleChange('password')}
            onBlr={formik.handleBlur('password')}
            val={formik.values.password}
          />
          <div className='error mt-2'>{formik.touched.password && formik.errors.password}</div>
          <div className='mb-3 text-end'>
            {/* <Link to='forgot-password' className=''>
              Forgot Password?
            </Link> */}
          </div>
          <button
            className='border-0 px-3 py-2 text-white fw-bold w-100 text-center text-decoration-none fs-5'
            style={{ background: '#ffd333' }}
            type='submit'
          >
            Login
          </button>
        </form>
      </div>
    </div>
  )
}

export default Login
