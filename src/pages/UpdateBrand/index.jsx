import { React, useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import 'react-quill/dist/quill.snow.css'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import * as yup from 'yup'
import { useFormik } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import './style.scss'
import CustomInput from '../../components/CustomInput'
import { storage } from '../../../firebaseConfig'
import { getDownloadURL, ref, uploadBytes } from 'firebase/storage'
import { v4 } from 'uuid'
import { getCurrentDateTimeFormatted } from '../../features/common'
import Loading from '../loading'
import { unwrapResult } from '@reduxjs/toolkit'
import { getItemsMethod, updateItemsMethod } from '../../features/product/middlewate'
let schema = yup.object().shape({
  name: yup.string().required('Name of Brand is Required')
})

const UpdateBrand = () => {
  const { id } = useParams()
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const [selectedImage, setSelectedImage] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const [avatar, setAvatar] = useState('')
  const { typeOfItems } = useSelector((state) => state.product)
  console.log(typeOfItems)

  useEffect(() => {
    const fetchData = async () => {
      const data = await dispatch(getItemsMethod({ type: typeOfItems, id }))
      setAvatar(data.payload.data.thumbnail)
      formik.setFieldValue('name', data.payload.data.name)
    }
    fetchData()
  }, [])

  const uploadImage = () => {
    return new Promise((resolve, reject) => {
      if (selectedImage === null) return reject('No image selected')
      const imageRef = ref(storage, `avatar/${v4() + '_' + selectedImage.name + '_' + getCurrentDateTimeFormatted()}`)

      uploadBytes(imageRef, selectedImage)
        .then((res) => {
          getDownloadURL(res.ref)
            .then((url) => {
              resolve(url)
            })
            .catch((e) => {
              reject(e)
            })
        })
        .catch((e) => {
          reject(e)
        })
    })
  }

  const formik = useFormik({
    initialValues: {
      name: ''
    },
    validationSchema: schema,
    onSubmit: (values) => {
      handleUpdateItem()
    }
  })

  const handleUpdateItem = async () => {
    setIsLoading(true)
    let url = null
    if (selectedImage) {
      // neu co chon anh moi
      try {
        url = await uploadImage()
      } catch (error) {
        console.log(error)
      }
    }
    try {
      const data = {
        name: formik.values.name,
        thumbnail: url ? url : avatar
      }
      const resultAction = await dispatch(updateItemsMethod({ params: { type: typeOfItems, id }, data: data }))
      if (resultAction.type.includes('rejected')) {
        toast.error(`Update item failed: ${resultAction.payload}`, {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          theme: 'light'
        })
        setIsLoading(false)
      } else {
        toast.success('Update item successful', {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          theme: 'light'
        })
        setIsLoading(false)

        setTimeout(() => {
          if (typeOfItems === 'brand') navigate('/admin/brands')
          if (typeOfItems === 'category') navigate('/admin/categories')
          if (typeOfItems === 'collection') navigate('/admin/collections')
        }, 1000)
      }
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div>
      <ToastContainer />
      {isLoading && <Loading />}
      <h3 className='mb-4 name'>Update {typeOfItems}</h3>
      <div>
        <img
          className='img-account-profile mb-2 img-fluid'
          src={
            selectedImage
              ? URL.createObjectURL(selectedImage)
              : avatar
                ? avatar
                : 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png'
          }
          alt='image'
        />
        <button className='btn btn-primary' type='button'>
          <label htmlFor='upload image'>Upload new image</label>
        </button>
        <input
          type='file'
          name='myImage'
          id='upload image'
          className='d-none'
          onChange={(event) => {
            setSelectedImage(event.target.files[0])
          }}
        />
        <form onSubmit={formik.handleSubmit} className='d-flex gap-3 flex-column'>
          <div className='w-50'>
            <CustomInput
              type='text'
              label='Enter Name'
              name='name'
              onChng={formik.handleChange('name')}
              onBlr={formik.handleBlur('name')}
              val={formik.values.name}
            />
            <div className='error'>{formik.touched.name && formik.errors.name}</div>
          </div>

          <div className='showimages d-flex flex-wrap gap-3'></div>
          <div className='d-flex gap-3 justify-content-center'>
            <button className='btn btn-primary' type='submit'>
              Update Brand
            </button>
            <button
              className='btn btn-secondary'
              type='button'
              onClick={() => {
                navigate(-1)
              }}
            >
              Back
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default UpdateBrand
