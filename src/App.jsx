import { useState } from 'react'
import { Routes, Route } from 'react-router-dom'
import MainLayout from './layout/MainLayout'
import Dashboard from './pages/Dashboard'
import Login from './pages/Login'
import Bloglist from './pages/Bloglist'
import Orders from './pages/Orders'
import Customers from './pages/Customers'
import Brandlist from './pages/BrandList'
import Addblog from './pages/AddBlog'
import Addproduct from './pages/AddProduct'
import AddCustomer from './pages/AddCustomer'
import UpdateCustomer from './pages/UpdateCustomer'
import AddBrand from './pages/AddBrand'
import UpdateBrand from './pages/UpdateBrand'
import Productlist from './pages/Productlist'

function App() {
  return (
    <Routes>
      <Route path='/' element={<Login />} />
      <Route path='/admin' element={<MainLayout />}>
        <Route index element={<Dashboard />} />
        <Route path='blog-list' element={<Bloglist />} />
        <Route path='blog' element={<Addblog />} />
        <Route path='orders' element={<Orders />} />
        <Route path='customers' element={<Customers />} />
        <Route path='customers/add' element={<AddCustomer />} />
        <Route path='customers/:username' element={<UpdateCustomer />} />
        {/* Apply for brand, category, collection */}
        <Route path='brands' element={<Brandlist />} />
        <Route path='brands/add' element={<AddBrand />} />
        <Route path='brands/:id' element={<UpdateBrand />} />
        <Route path='collections' element={<Brandlist />} />
        <Route path='collections/add' element={<AddBrand />} />
        <Route path='collections/:id' element={<UpdateBrand />} />
        <Route path='categories' element={<Brandlist />} />
        <Route path='categories/add' element={<AddBrand />} />
        <Route path='categories/:id' element={<UpdateBrand />} />

        <Route path='products' element={<Productlist />} />
        <Route path='products/add' element={<Addproduct />} />
      </Route>
    </Routes>
  )
}

export default App
