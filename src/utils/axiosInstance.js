import axios from 'axios';
import queryString from 'query-string'
import { getLocalStorage } from './localStorage';

export const createAxiosInstance = (baseURL) => {
  const instance = axios.create({
    withCredentials: false,
    baseURL: baseURL,
    timeout: 5000,
    headers: {
      "Content-type": "application/json",
    },
    paramsSerializer: (params) => queryString.stringify(params)
  });

  instance.interceptors.request.use((request) => {
    // // Check if the request is for logging in
    // const isLoginRequest = request.url.includes('/user/auth/admin/login'); // Modify the condition based on your login endpoint

    // // Add Authorization header only for non-login requests
    // if (!isLoginRequest) {
    //   const accessToken = getLocalStorage("accessToken");
    //   const accessHeader = `Bearer ${accessToken}`;
    //   if (request.headers) request.headers['Authorization'] = accessHeader;
    // }

    return request;
  });

  return instance;
};