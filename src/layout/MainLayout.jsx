import React, { useEffect, useState } from 'react'
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons'
import { AiOutlineDashboard, AiOutlineShoppingCart, AiOutlineUser, AiOutlineBgColors } from 'react-icons/ai'
import { MdCollections } from "react-icons/md";
import { RiCouponLine } from 'react-icons/ri'
import { ImBlog } from 'react-icons/im'
import { IoIosNotifications } from 'react-icons/io'
import { FaClipboardList, FaBloggerB } from 'react-icons/fa'
import { SiBrandfolder } from 'react-icons/si'
import { BiCategoryAlt } from 'react-icons/bi'
import { Layout, Menu, Button, theme } from 'antd'
import { Link, useNavigate } from 'react-router-dom'
const { Header, Sider, Content } = Layout
import { Outlet } from 'react-router-dom'
import './style.scss'
import { ToastContainer } from 'react-toastify'
import { useDispatch, useSelector } from 'react-redux'
import { setSelectedKey } from '../features/customer/customerSlice'

function MainLayout() {
  const [collapsed, setCollapsed] = useState(false)
  const { selectedKey } = useSelector((state) => state.customer)
  const dispatch = useDispatch()
  const {
    token: { colorBgContainer }
  } = theme.useToken()
  const nav = useNavigate()

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className='demo-logo-vertical'>
          <h2 className='text-white fs-5 text-center py-3 mb-0'>
            <span className='sm-logo'>TS</span>
            <span className='lg-logo'>T Store</span>
          </h2>
        </div>
        <Menu
          theme='dark'
          mode='inline'
          selectedKeys={[selectedKey]}
          onClick={({ key }) => {
            if (key == 'signout') {
            } else {
              dispatch(setSelectedKey(key))
              nav(key)
            }
          }}
          items={[
            {
              key: '',
              icon: <AiOutlineDashboard className='fs-4' />,
              label: 'Dashboard'
            },
            {
              key: 'customers',
              icon: <AiOutlineUser className='fs-4' />,
              label: 'Customers'
            },
            {
              key: 'Products',
              icon: <AiOutlineShoppingCart className='fs-4' />,
              label: 'Products',
              children: [
                {
                  key: 'products',
                  icon: <AiOutlineShoppingCart className='fs-4' />,
                  label: 'List Product'
                },
                {
                  key: 'brands',
                  icon: <SiBrandfolder className='fs-4' />,
                  label: 'List Brand'
                },
                {
                  key: 'categories',
                  icon: <BiCategoryAlt className='fs-4' />,
                  label: 'List Category'
                },
                {
                  key: 'collections',
                  icon: <MdCollections className='fs-4' />,
                  label: 'List Collection'
                }
              ]
            },
            {
              key: 'orders',
              icon: <FaClipboardList className='fs-4' />,
              label: 'Orders'
            },
            {
              key: 'blogs',
              icon: <FaBloggerB className='fs-4' />,
              label: 'Blogs',
              children: [
                {
                  key: 'blog',
                  icon: <ImBlog className='fs-4' />,
                  label: 'Add Blog'
                },
                {
                  key: 'blog-list',
                  icon: <FaBloggerB className='fs-4' />,
                  label: 'Blog List'
                }
              ]
            }
          ]}
        />
      </Sider>
      <Layout className='site-layout'>
        <Header
          className='d-flex justify-content-between ps-1 pe-5'
          style={{
            padding: 0,
            background: colorBgContainer
          }}
        >
          {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: 'trigger',
            onClick: () => setCollapsed(!collapsed)
          })}
          <div className='d-flex gap-4 align-items-center'>
            <div className='position-relative'>
              <IoIosNotifications className='fs-4' />
              <span className='badge bg-warning rounded-circle p-1 position-absolute'>3</span>
            </div>

            <div className='d-flex gap-3 align-items-center dropdown'>
              <div>
                <img
                  width={32}
                  height={32}
                  src='https://firebasestorage.googleapis.com/v0/b/uploadimagetest-d03e3.appspot.com/o/image%2F17566e7e-0e42-43cb-8be5-ab704fa4cf86Nguy%E1%BB%85n%20M%E1%BA%A1nh%20Tu%C3%A2n%20(1)%20(1).JPG?alt=media&token=b3b3dfa4-f1e7-44df-b2d3-22286d402d28'
                  alt=''
                />
              </div>
              <div role='button' id='dropdownMenuLink' data-bs-toggle='dropdown' aria-expanded='false'>
                <h5 className='mb-0'>tuan112</h5>
                <p className='mb-0'>manhtuan1122001@gmail.com</p>
              </div>
              <div className='dropdown-menu' aria-labelledby='dropdownMenuLink'>
                <li>
                  <Link className='dropdown-item py-1 mb-1' style={{ height: 'auto', lineHeight: '20px' }} to='/'>
                    View Profile
                  </Link>
                </li>
                <li>
                  <Link className='dropdown-item py-1 mb-1' style={{ height: 'auto', lineHeight: '20px' }} to='/'>
                    Signout
                  </Link>
                </li>
              </div>
            </div>
          </div>
        </Header>
        <Content
          style={{
            margin: '24px 16px',
            padding: '0 24px 24px 24px',
            minHeight: 280,
            background: colorBgContainer
          }}
        >
          <ToastContainer
            position='top-right'
            autoClose={250}
            hideProgressBar={false}
            newestOnTop={true}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            theme='light'
          />
          <Outlet />
        </Content>
      </Layout>
    </Layout>
  )
}

export default MainLayout
