import { configureStore } from '@reduxjs/toolkit';
import userReducer from '../features/authenticate/userSlice'
import customerReducer from '../features/customer/customerSlice'
import productReducer from '../features/product/productSlice'


export const store = configureStore({
  reducer:{
    user: userReducer,
    customer: customerReducer,
    product: productReducer
  }
});